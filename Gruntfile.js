module.exports = function(grunt) {
    
        // Project configuration.
        grunt.initConfig({
            sass: {
                dist: {
                    options: {
                        style: 'expanded'
                    },
                    files: {
                        './css/main.css': './scss/main.scss' // 'destination': 'source'           
                    }
                }
            },
    
    
            watch: {
                css: {
                    files: './scss/main.scss',
                    tasks: ['sass'],
                }
            }
        });
    
    
        // Load the plugin that provides the "uglify" task.
        grunt.loadNpmTasks('grunt-contrib-sass');
        grunt.loadNpmTasks('grunt-contrib-watch');
    
        // Default task(s).
        grunt.registerTask('default', ['sass']);
    };